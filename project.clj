(defproject khroma-tests "0.2.0"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.145"]
                 [devcards "0.2.0-5"]
                 [khroma "0.2.0"]]
  :source-paths ["src"]
  :profiles {:dev
             {:plugins [[lein-cljsbuild "1.1.0"]
                        [lein-chromebuild "0.3.1"]]
              :cljsbuild
                       {:builds
                        {:main
                         {:source-paths ["src"]
                          :compiler     {:output-to     "target/unpacked/khroma_tests.js"
                                         :output-dir    "target/js"
                                         :optimizations :whitespace
                                         :main          "khroma-tests.main-tests"
                                         :devcards      true
                                         :pretty-print  true}}}}}})
